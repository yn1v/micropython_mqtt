import esp
esp.osdebug(None)

import gc
gc.collect()
import main
import config
#exec(open('./main.py').read(),globals())

ssid = config.ssid
password = config.password

def connect():
    import network
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print('connecting to network...')
        sta_if.active(True)
        sta_if.connect(ssid, password)
        while not sta_if.isconnected():
            pass
    print('network config:', sta_if.ifconfig())

connect()



