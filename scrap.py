import requests
import math
import paho.mqtt.client as mqtt
from time import sleep

def truncate(number, digits) -> float:
    stepper = 10.0 ** digits
    return math.trunc(stepper * number) / stepper

def hume():
    urlh = "http://192.168.88.34/humidity"
    rh = requests.get(urlh, allow_redirects=True)
    humidity = int(rh.content[-3:-1])
    return str(humidity)

def tempe():
    urlt = "http://192.168.88.34/temp"
    rt = requests.get(urlt, allow_redirects=True)
    temp = (rt.content[-5:-2])
    temp = (int(temp)-32)*5/9
    return str(truncate(temp, 2))

cid="iotmcu"
trans="tcp"
c_ses=True

mqttc = mqtt.Client(client_id=cid, clean_session=c_ses, transport=trans)

server = "zancudo.sytes.net"
pt = 1883

while True:
    mqttc.connect(server, port=pt)
    t_tempe = "test/temp"
    t_hume = "test/humidity"
#   t_tempe = "villafontana/cuarto/temp"
#   t_hume = "villafontana/cuarto/humidity"
    pl_t = tempe()
    pl_h = hume()
    mqttc.publish(t_tempe, payload=pl_t)
    print(t_tempe, pl_t)
    mqttc.publish(t_hume, payload=pl_h)
    print(t_hume, pl_h)
    mqttc.disconnect()
    sleep(60)
