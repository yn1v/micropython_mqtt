import dht
import machine
from time import sleep
from umqtt.robust import MQTTClient

htsensor = dht.DHT11(machine.Pin(0))

# server = "zancudo.sytes.net"
server = "192.168.96.197"
client_id = "1234-5678-9012-3456"
client = MQTTClient(client_id, server)

# topics
top_temp = "teodolinda/gerencia/temp"
top_hum = "teodolinda/gerencia/humidity"

print ("esperando que el sensor se estabilice")
sleep(5)

while True:
    print("leyendo datos")
    htsensor.measure()
    print("conectando")
    client.connect()
    temperatura = str(htsensor.temperature())
    print("publicando temperatura:", temperatura)
    client.publish(top_temp, temperatura)
    sleep(1)
    humedad = str(htsensor.humidity())
    print("Publicando humedad:", humedad) 
    client.publish(top_hum, humedad)
    sleep(1)
    print("desconectando")
    client.disconnect()
    sleep(28)




